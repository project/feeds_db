<?php

/**
 * @file
 * Home of the DBFetcher and related classes.
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * DBFetcher.
 */
class DBFetcherResult extends FeedsFetcherResult {

  /**
   * Overrides parent::getRaw();
   */
  public function getRaw() {
    return unserialize($this->sanitizeRaw(serialize($this->raw)));
  }

}

/**
 * Fetches data via DB.
 */
class DBFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);

    // @TODO: Think about the environment, implement batch here please.

    if ($source_config['limit'] <= 0) {
      db_set_active($source_config['database']);
      $query = db_query($source_config['database_query']);
      db_set_active('default');
      $result = $query->fetchAll();
      return new DBFetcherResult($result);
    }

    // Batch it.
    $state = $source->state(FEEDS_FETCH);
    if (!isset($state->num_rows)) {
      db_set_active($source_config['database']);
      $state->num_rows = $state->total = db_query('SELECT count(*) FROM (' . $source_config['database_query'] . ') as records')->fetchField();
      db_set_active('default');
    }
    if ($state->num_rows) {
      db_set_active($source_config['database']);
      $query = db_query_range($source_config['database_query'], $state->total - $state->num_rows, $source_config['limit']);
      db_set_active('default');
      $state->num_rows -= $query->rowCount();
      $result = $query->fetchAll();
      $state->progress($state->total, $state->total - $state->num_rows);
      return new DBFetcherResult($result);
    }
  }

  /**
   * Overrides parent:sourceDefaults()
   */
  public function sourceDefaults() {
    global $databases;
    $db_names = array();
    foreach (array_keys($databases) as $db_name) {
      $db_names[$db_name] = $db_name;
    }
    unset($db_names[array_search('default', $db_names)]);
    return array(
      'database' => current($db_names), 
      'database_query' => '',
      'limit' => 0,
    );
  }
    
  /**
   * Source form.
   */
  public function sourceForm($source_config) {
    global $databases;
    $form = array();
    $db_names = array();
    foreach (array_keys($databases) as $db_name) {
      $db_names[$db_name] = $db_name;
    }
    $source_config += $this->sourceDefaults();
    unset($db_names[array_search('default', $db_names)]);
    $form['database'] = array(
      '#type' => 'select',
      '#title' => t('Database'),
      '#options' => $db_names,
      '#required' => TRUE,
      '#default_value' => $source_config['database'],
    );

    $form['database_query'] = array(
      '#type' => 'textarea',
      '#title' => t('Database query'),
      '#description' => t('The SQL query to run on the database. Be sure to include all necessary fields.'),
      '#required' => TRUE,
      '#default_value' => $source_config['database_query'],
    );

    $form['limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Limit'),
      '#description' => t('Specify the limit of nodes to be processed withing a single request. Use it wisely to save memory on big tables. Leave it as 0 for no limit.'),
      '#default_value' => $source_config['limit'],
    );
    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
  }

  /**
   * Override parent::sourceSave().
   */
  public function sourceSave(FeedsSource $source) {
  }

}
